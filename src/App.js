import React, { useState } from 'react';

import Header from './components/Header';
import Input from './components/Input';
import TodoList from './components/TodoList';
import './App.css';

const App = () => {
  const [todolist, setState] = useState([
    { id: '0', text: 'Study Javascript', iscompleted: true },
    { id: '1', text: 'Example With Javascript', iscompleted: true }
  ]);

  const addTodo = todo => {
    setState([...todolist, todo]);
  };

  const deleteTodo = id => {
    const todolistAfterDelete = todolist.filter(todo => todo.id !== id);

    setState([...todolistAfterDelete]);
  };

  const completedTodo = id => {
    const todolistAfterCompleted = todolist.map(todo => {
      if (todo.id === id) {
        todo.iscompleted = !todo.iscompleted;
      }

      return todo;
    });

    setState([...todolistAfterCompleted]);
  };

  return (
    <div className="App">
      <Header />
      <Input addTodo={addTodo} />
      <TodoList
        todolist={todolist}
        deleteTodo={deleteTodo}
        completedTodo={completedTodo}
      />
    </div>
  );
};

export default App;
