import React from 'react';

import './TodoItem.css';

const TodoItem = props => {
  const { item, deleteTodo, completedTodo } = props;

  const onDeleteClick = id => {
    deleteTodo(id);
  };

  const onCompletedClick = id => {
    completedTodo(id);
  };

  return (
    <div className="todo-item">
      <p
        className={item.iscompleted === true ? 'is-completed' : 'not-completed'}
      >
        {item.text}
      </p>
      <button
        className="btn-completed"
        onClick={() => onCompletedClick(item.id)}
      >
        completed
      </button>
      <button className="btn-delete" onClick={() => onDeleteClick(item.id)}>
        delete
      </button>
    </div>
  );
};

export default TodoItem;
